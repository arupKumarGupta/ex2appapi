/**
 * @author Arup Kumar Gupta
 * @email akgupta@ex2india.com
 * @create date 2018-12-01 11:42:55
 * @modify date 2018-12-01 11:42:55
 * @desc User routes controller
*/

'use-strict';
/*
    * the api routes BL are written here
    *each route will send a json in the give below format
    *response = {
        success: True/False, 
        message: String
        data: {}/[]
    }
    *? being optional and atleast any one of them is needed to be returned in response
    *res.json(response)
 */
const _ = require('lodash');
const {Auth} = require('../middleware');
const {User,UserRole,Role} = require('../db');
const messages = require('../MessageDictionary.json');
const path = require('path');
const MailerService = require(path.join(__dirname,'..','utility','mailer-service'));
const url = require('url');
function isEmptyOject(obj){
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}
class UserController{
    static async getAllUsers(req,res){
        try{
            let users = await User.findAll({
                include:User.includeOptions
            });
            return res.json({success:true,data:users});
        }catch(err){
            console.log(err);
            return res.status(500).json({success:false,message:messages.serverError});
        }        
    }
    static async getUser(req,res){
        let pattern = req.params['pattern'];
        try{
            let users = await User.findAll({
                where:{
                    $or: [
                       {
                        email:{
                            $like: [`%${pattern}%`]
                        }
                       },
                       {
                        first_name:{
                            $like: [`%${pattern}%`]
                        }
                       },
                       {
                        last_name:{
                            $like: [`%${pattern}%`]
                        }
                       }
                    ]
                },
                include:User.includeOptions
            });
            let searchResult = [];
            users.forEach(function(user){
                searchResult.push(user.toJSON());
            });
            res.json({success:true,data:searchResult});

        }catch(e){
            console.log(e);
            return res.status(500).json({success:false,message:messages.serverError});
        }
       
    }
    static async createNewUser(req,res){
        let userObj = _.pick(req.body,[
            'email',
            'password',
            'first_name',
            'last_name',
            'employee_code',
            'mobile_number'
        ]);
        let userRole = req.body['role'];
        try{
            let currentUser = req.user;
           // fetch valid role from database
           let roles =  await Role.findAll({
               where:{role_value:userRole}
           });
           if(!roles.length)
            return res.status(406).json({success:false,message:messages.invalidRole});
           let userExists = await User.count({where:{
               email:userObj.email
           }});
           if(userExists)
            return res.json({success:false,message: messages.alreadyTaken})
           let user = await User.create(userObj);
           let role = roles[0];
           //add entry to userRole
           await UserRole.create({role_id:role.id,user_id:user.id,created_by:currentUser.id});
           let jsonUser = await User.findOne({options:{id:user.id}});
           return res.json({success:true,data:jsonUser});
        }catch(insertError){
            console.log(insertError);
            return res.status(500).json({success:false,message:messages.serverError});
        }
    }
    static async deleteUser(req,res){
        res.json({success:false,message:'Method not implemented'});
    }
    static async updateUser(req,res){
        res.json({success:false,message:'Method not implemented'});
    }
    static async login(req,res){
        let userCreds = _.pick(req.body,['email','password']);
            try{
                let user = await User.findOne({
                    options:{
                        email:userCreds.email
                    }
                });
                if(!user || isEmptyOject(user)){
                    return res.status(404).json({success:false,message:messages.noUserFound});
                }
                let verifiedUser = await Auth.compare(userCreds.password,user.password);
                if(!verifiedUser)
                    return res.status(401).json({success:false, message:messages.invalidCredError});
                //generate auth token
                let token = await Auth.generateAuthToken(user.toJSON());
                user.token = token;
                user.last_logged_on = new Date();
                await user.save();
                res.header('x-auth',user.token).status(200).json({success:true,data:user.toJSON(),auth_token:token});
            }catch(e){
                console.log(e);
                return res.status(403).json({success:false,message:messages.forbiddenError});
            }  
    }

    static async logout(req,res){
        try{
            // console.log(req.user);
            if(req.user == {})
                return res.json({success:false,message:messages.unauthorizedError});
            req.user.removeToken();
            await req.user.save();
            res.json({success:true,message:'Logged out'});
        }catch(err){
            console.log(err);
            res.status(500).json({success:false,message:messages.serverError});
        }
    }
    static async updateProfile(req,res){
        const updateUser = _.pick(req.body,['current_password','password','mobile_number']);
        try{
            let user = req.user;
            

            let verifiedUser = await Auth.compare(updateUser.current_password,user.password);
            if(!verifiedUser){
                return res.status(401).json({success:false,message:messages.invalidCredError});
            }
            user.password = updateUser.password;
            await user.save();
            let token = await Auth.generateAuthToken(user);
            user.token = token;
            let updatedUser = await user.save();
            return res.json({success:true,data:updatedUser});
        }catch(err){
            console.log(err);
            return res.status(500).json({success:false,message:messages.serverError});
        }
    }
    static async viewProfile(req,res){
        let userId = req.user.id;
        try{
            let userRole = await UserRole.findOne({
                options: {
                    user_id:userId                
                }
            });
            let roleId = userRole.role_id;
            let role = await Role.findById(roleId);
            let user = req.user.toJSON();
            user.role = role.role_value;
            return res.json({success:true,data:user});
        }catch(err){
            console.log(err);
            return res.status(500).json({success:false,message:messages.serverError});
        }
    }
    static async sendConfirmationMail(req,res){
        const confimationUrl = `${req.host}:${process.env.port||3000}${req.baseUrl}/activate`;
        let idString = req.params['idString'];
        let ids = idString.split(',');
        let mailerService = new MailerService();
        let activationLinks = [];
        let users = [];
        for(let id of ids){
            let userId = +id;//casting to int
            try{
                let user = await User.findOne({
                    options:{
                        id:userId,
                        is_active: false
                    }
                });
                if(user && !isEmptyOject(user)){
                    let activationToken = await Auth.generateAuthToken(user,'1d');
                    let activateUrl = `${confimationUrl}/${activationToken}`;
                    activationLinks.push(activateUrl);
                    mailerService.sender='noreply@ex2india.com';
                    mailerService.reciever= user.email;
                    mailerService.mailSubject = 'Activate Your Account';
                    mailerService.HTML=`
                    Hi ${user.first_name},<br>
                    Please activate your account by clicking the link below.<br>
                    <a href="${activateUrl}">${activateUrl}</a><br>
                    Thank you
                    Ex2 India
                    `;
                    mailerService.sendMail().then((d)=>{
                        console.log(d);
                    }).catch(e=>{
                        console.log(e.message);
                    })
                }

                }catch(e){
                return res.status(500).json({success:false,message:e.message});
            }
         
        }
        //TODO implement nodemailer here for email confimation
            //generate activation token
        
        res.json({success:true,data:activationLinks});

    }

    static async activateUser(req,res){
        let activationToken = req.params['activation_token'];
        try{
            let decoded = await Auth.verifyAuthToken(activationToken);
            let user = await User.findOne({
               options:{email:decoded.user.email}
            });
            if(!user || isEmptyOject(user)){
                return res.status(404).json({success:false,message:messages.noUserFound});
            }
            user.is_active = true;
            await user.save();
            res.json({success:true,data:user,reset_token:activationToken});
        }catch(e){
            res.json({success:false,message:e.message});
        }
    }

    static async forgotPassword(req,res){
        let email = req.body.email;
        let user = null;
        try{
            user = await User.findOne({
                options:{email}
            }); 
            if(!user || isEmptyOject(user))
                return res.status(404).json({success:false, message:messages.noUserFound});
            let reset_token = await Auth.generateAuthToken(user.toJSON(),'1d'); // expiration of 1d
            res.json({success:true,data:user,reset_token});
        }catch(e){
            res.status(500).json({success:false,message:e.message});
        }
    }
    static async resetPassword(req,res){
        let token = req.params['reset_token'];
        try{
            let decoded = await Auth.verifyAuthToken(token);
            let email = decoded.user.email;
            try{
                let user = await User.findOne({options:{email}});
                if(!user || isEmptyOject(user))
                    return res.status(404).json({success:false,message:messages.noUserFound});
                let password = _.pick(req.body,['password']).password;
                user.password = password;
                user.token = '';
                let updatedUser = await user.save();
                res.json({success:true,data:updatedUser});
            }catch(e){
                // console.log(e);
                 res.status(500).json({success:false,message:e})};
        }catch(e){
            res.status(500).json({success:false,message:e});
        }
    }
}

module.exports = UserController;