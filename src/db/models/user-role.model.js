/**
 * @author Arup Kumar Gupta
 * @email akgupta@ex2india.com
 * @create date 2018-12-01 11:44:38
 * @modify date 2018-12-01 11:44:38
 * @desc UserRoles Model
*/

const { connection, Sequelize } = require('../connection');
const Role = require('./role.model');
const User = require('./user.model');
const UserRole = connection.define('UserRole',{});
UserRole.findOne = async function (options) {
    try {
        let results = await this.findAll({ limit: 1, where: options.options, attributes: options.attributes });
        return new Promise((resolve, reject) => {
            if (!results.length)
                return resolve({});
            return resolve(results[0]);
        });
    } catch (err) { console.log(err); return Sequelize.Promise.reject('Server Error'); }
}
UserRole.belongsTo(Role,{foreignKey:'role_id'});
// UserRole.belongsTo(User,{foreignKey:'user_id'});
// UserRole.belongsTo(User,{foreignKey:'created_by',as:'creator'});

module.exports = UserRole;