/**
 * @author Arup Kumar Gupta
 * @email  akgupta@ex2india.com
 * @create date 2018-12-06 21:24:32
 * @modify date 2018-12-06 21:24:32
 * @desc Item Model
*/

const {connection, Sequelize} = require('../connection');
//ItemType
const ItemType = connection.define('ItemType',{
    value:Sequelize.STRING(100),
    is_active: Sequelize.BOOLEAN
});

//Item
const Item = connection.define('Item',{
    item_name: {
        type: Sequelize.STRING(200),
        allowNull:false,
    },
    serial__number:{
        type: Sequelize.STRING(100),
        allowNull:false
    },
    version: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    storage: {
        type: Sequelize.STRING(100),
        allowNull:false
    },
    price:{
        type: Sequelize.DOUBLE,
        allowNull: false,
    },
    purchase_date: {
        type: Sequelize.DATE,
        allowNull: false
    },
    created_by:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    item_type_id:{
        type: Sequelize.INTEGER,
    },
    assigned_to_user_id:{
        type: Sequelize.INTEGER
    },
    possession_date: {
        type: Sequelize.DATE
    },
    is_active:{
        type:Sequelize.BOOLEAN,
        defaultValue: true
    }
});

Item.findOne = async function (options) {
    try {
        let results = await this.findAll({ limit: 1, where: options.options, attributes: options.attributes });
        return new Promise((resolve, reject) => {
            if (!results.length)
                return resolve({});
            return resolve(results[0]);
        });
    } catch (err) { console.log(err); return Sequelize.Promise.reject('Server Error'); }
}

module.exports = {ItemType,Item};