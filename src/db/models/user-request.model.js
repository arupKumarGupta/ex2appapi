/**
 * @author Arup Kumar Gupta
 * @email  akgupta@ex2india.com
 * @create date 2018-12-06 22:12:30
 * @modify date 2018-12-06 22:12:30
 * @desc User Request Model
*/
const {connection, Sequelize} = require('../connection');
const UserRequest = connection.define('UserRequest',{
    item_id: Sequelize.INTEGER,
    request_date: Sequelize.DATE,
    from_user_id: Sequelize.INTEGER,
    to_user_id: Sequelize.INTEGER,
    last_updated_date: Sequelize.DATE,
    action_by: Sequelize.INTEGER,
    status_id: Sequelize.INTEGER,
    comment: Sequelize.STRING(200),
    response: Sequelize.STRING(100)
});
UserRequest.findOne = async function (options) {
    try {
        let results = await this.findAll({ limit: 1, where: options.options, attributes: options.attributes });
        return new Promise((resolve, reject) => {
            if (!results.length)
                return resolve({});
            return resolve(results[0]);
        });
    } catch (err) { console.log(err); return Sequelize.Promise.reject('Server Error'); }
}
module.exports = UserRequest;