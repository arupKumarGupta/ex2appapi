/**
 * @author Arup Kumar Gupta
 * @email  akgupta@ex2india.com
 * @create date 2018-12-06 22:09:48
 * @modify date 2018-12-06 22:09:48
 * @desc Status Model
*/
const {connection, Sequelize} = require('../connection');
const Status = connection.define('Status',{
    value :Sequelize.STRING(100)
});
module.exports = Status;