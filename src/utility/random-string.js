const randomString = function (length = 16) {
    const base = 'abcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()';
    var a = base.split(""),
        n = a.length;
    for (var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("").substring(0, length);
}
module.exports = randomString;