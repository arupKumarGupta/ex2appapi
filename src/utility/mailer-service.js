const nodemailer = require('nodemailer');
const CONFIG = {
    MAILGUN_USER:'postmaster@sandboxc250f6749c6e4d9ba460c6d6a6ae9df1.mailgun.org',
    MAILGUN_PASS:'df479e6d46e01858a84e95d49108d689-41a2adb4-934bbbb7'
}
class MailerService {
    
    constructor(){
        this.transport = nodemailer.createTransport({
            host: 'localhost',
            port: 25,
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false
            }
        });
        this.from = '';
        this.to = '';
        this.subject = '';
        this.html = '';
    }
    set sender(senderEMail){
        this.to = senderEMail;
    }
    set reciever(reciever){
        this.to = reciever;
    }
    set mailSubject(subject){
        this.subject = subject;
    }
    set HTML(html){
        this.html = html;
        return this;
    }
    sendMail(){
        let mailOptions = {
            from : this.from,
            to:this.to,
            subject:this.subject,
            html:this.html
        }
        return  this.transport.sendMail(mailOptions);
    }
}

// let mailerService = new MailerService();
/* mailerService
            .sender('noreply@arup.com')
            .reciever('akgupta@ex2india.com,arup.shibai@gmail.com')
            .addSubject('Test Mail')
            .addHTML('Hello Again From Node Mailer')
            .sendMail().then((d)=>{console.log(d)}).catch(err=>console.log(err)); */

module.exports = MailerService;