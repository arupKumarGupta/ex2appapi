const fs = require('fs');
const csv = require('fast-csv');
const path = require('path');
const randomString = require('./random-string');
const { connection, User, UserRole, Role } = require('../db');
connection.sync().then(async () => {
    var role = null;
    try {
         role =  await Role.findOne({options:{role_value:'user'}});
         role = await Role.create({
            role_value: 'user'
        });
    } catch (e) {
        //may be already exists
    }
    //reading from csv and creating entry in db
    fs.createReadStream(path.join(__dirname, 'usersCSV.csv'))
    .pipe(csv())
    .on('data', async (data) => {
        let userObj = {
            employee_code: data[0],
            first_name: data[1],
            last_name: data[2],
            email: data[3],
            password: randomString(8)
        };
        try {
            await connection.sync();
            let user = await User.create(userObj);
            await UserRole.create({ role_id: role.id, user_id: user.id, created_by: 1 });
        } catch (e) {
            console.log(e);
        }
    })
    .on('end', () => {
        console.log('finished Reading csv!');
    });
}).catch((err) => {
    console.log(err.message);
});
